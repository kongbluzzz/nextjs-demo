"use client"
import * as React from 'react'
import dayjs, { Dayjs } from 'dayjs'
import { DemoContainer } from '@mui/x-date-pickers/internals/demo'
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider'
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs'
import { DatePicker } from '@mui/x-date-pickers/DatePicker'
import { Box, width } from '@mui/system'
import { Divider, Grid, Paper, Stack, TextField, Typography, styled } from '@mui/material'
import { Button } from "@mui/material"

import { StatusPopup } from '@/component/client/StatusPopup'
import { FormBoxProp } from '@/type/formBoxProp'
import { customRequest } from '@/util/customRequest'

const Item = styled(Paper)(({ theme }) => ({
    backgroundColor: theme.palette.mode === 'dark' ? '#1A2027' : '#fff',
    ...theme.typography.body2,
    padding: theme.spacing(1),
    textAlign: 'center',
    color: theme.palette.text.secondary,
}))

export function FormBox(
    props: {
        title: string,
        formBoxProps: Array<FormBoxProp>,
        apiPath: string,
    }
) {
    const {title, formBoxProps, apiPath} = props
    const [successStatus, setSuccessStatus] = React.useState<boolean>(false)
    const [errorStatus, setErrorStatus] = React.useState<boolean>(false)

    let formState = {}
    formBoxProps.forEach((formBoxProp) => {
        const defaultValue = setDefaultStateValue(formBoxProp.default, formBoxProp.type)
        const [value, setValue] = React.useState(defaultValue)
        formState[formBoxProp.name] = {
            value,
            setValue
        }
    })

    function hamdleDateChange(setValue) {
        return  (newDate) => {setValue(newDate)}
    }

    function handleDataChange(setValue) {
        return  (event) => {setValue(event.target.value)}
    }

    async function submitForm() {
        const data = {}
        Object.keys(formState).forEach(
            (key: string) => {
                data[key] = formState[key].value
            }
        )

        const response = await customRequest.post(apiPath, data, 5)
        if (response) {
            setSuccessStatus(true)
        } else {
            setErrorStatus(true)
        }
    }

    return (
        <>
        <Box
            bgcolor={"white"}
            border={2}
            width={"400px"}
        >
            <Stack>
                <Item>   
                    <Typography variant='h5'>{title}</Typography>
                </Item>
                {
                    formBoxProps.map((formBoxProp) => {
                        const value = formState[formBoxProp.name].value
                        const setValue = formState[formBoxProp.name].setValue
                        const handleChange = handleDataChange(setValue)
                        
                        
                        if (formBoxProp.type === "string" | formBoxProp.type === "float") {
                            return (
                                <StringInput 
                                    display={formBoxProp.display}
                                    value={value} 
                                    onChange={handleChange} 
                                />
                            )
                        } else if (formBoxProp.type === "date") {
                            return (
                                <DatePickerInout 
                                    display={formBoxProp.display}
                                    value={value}
                                    onChange={handleChange}
                                />
                            )
                        }
                    })
                }
                <Grid container
                    p={1}
                    justifyContent={"right"}
                >
                    <Grid item xs={3}
                        justifyContent={"center"}
                        display={'flex'}
                        
                    >
                        <Button 
                            variant='contained'
                            type='submit'
                            onClick={submitForm}
                        >
                            <Typography > {"Add"} </Typography>
                        </Button>
                    </Grid>
                </Grid> 
            </Stack>
        </Box>
        <StatusPopup
            open={successStatus}
            setOpen={setSuccessStatus}
            message={"บันทึกรายการสำเร็จ"}
            color={"#5cb85c"}
        / >
        <StatusPopup
            open={errorStatus}
            setOpen={setErrorStatus}
            message={"เกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้ง"}
            color={"#d9534f"}
        / >
        </>
    )
}

function DatePickerInout(props: {display, value, onChange}) {
    const {display, value, onChange} = props

    return (
        <Grid container 
            p={2}
            justifyItems={"left"}
            textAlign={"left"}
            alignItems={'center'}
        >
            <Grid item xs={3}>
                <Typography>{display}</Typography>
            </Grid>
            <Grid item xs={9}>
                <LocalizationProvider dateAdapter={AdapterDayjs}>
                    <DemoContainer 
                        components={['DatePicker', 'DatePicker']}
                    >
                        <DatePicker
                            name={"date"}
                            value={value}
                            onChange={onChange}
                        />
                    </DemoContainer>
                </LocalizationProvider>
            </Grid>
        </Grid>
    )
}

function StringInput(props: {display: string, value, onChange}) {
    const {display, value, onChange} = props
    
    return (
        <Grid container
            p={2}
            justifyItems={"left"}
            textAlign={"left"}
            alignItems={'center'}
        >
            <Grid item xs={3}>
                <Typography> {display} </Typography>
            </Grid>
            <Grid item xs={9}>
                <TextField 
                    name="detail"
                    value={value}
                    onChange={onChange}
                />
            </Grid>
        </Grid>
    )
}

function setDefaultStateValue(defaultValue, valueType) {
    let autoDefault = undefined
    if (valueType === "date") {
        autoDefault = dayjs()
    } else if (valueType === "string") {
        autoDefault = ""
    }

    return defaultValue ? defaultValue : autoDefault
}