export type FormBoxProp {
    name: string,
    display: string,
    type: "date" | "string" | "float",
    default?: any,
}
