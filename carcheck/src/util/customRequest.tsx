"use client"

async function post(apiPath: string, data: object, timeout: number) {
    try {
        const response = await fetch(
            apiPath,
            {
                method: "POST",
                body: JSON.stringify(data),
            },
        )

        if (response.ok) {
            return response.json()
        } else {
            return undefined
        }
    } catch {
        return undefined
    }
}

export const customRequest = {
    post
}
