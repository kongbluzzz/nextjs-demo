build-develop:
	docker compose build 
	docker compose run --rm carcheck yarn install

start-develop:
	docker compose up -d
