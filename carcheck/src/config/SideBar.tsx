import { ReactNode } from "react"
import AddToPhotosIcon from '@mui/icons-material/AddToPhotos';
import IndeterminateCheckBoxIcon from '@mui/icons-material/IndeterminateCheckBox';
import WalletIcon from '@mui/icons-material/Wallet';
import MopedIcon from '@mui/icons-material/Moped';

type SideBar = {
    name: string
    icon:  ReactNode
    link: string
}


export const SideBarSettings: Array<SideBar> = [
    {
        name: "รายรับ",
        icon: (<AddToPhotosIcon />),
        link: "/revenue"
    },
    {
        name: "รายจ่าย",
        icon: (<IndeterminateCheckBoxIcon />),
        link: "/expense"
    },
    {
        name: "ตรวจรถ",
        icon: (<MopedIcon />),
        link: "/carCheckUpDetail"
    },
    {
        name: "สรุปบัญชี",
        icon: (<WalletIcon />),
        link: "/financial"
    },
]