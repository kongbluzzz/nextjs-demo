"use-client"
import * as React from 'react';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Snackbar, { SnackbarOrigin } from '@mui/material/Snackbar';
import { SnackbarContent, Typography } from '@mui/material';

interface State extends SnackbarOrigin {
  open: boolean;
}

export function StatusPopup(props: {
  open: boolean,
  setOpen ,
  message: string, 
  color: string
}) {
    const {open, setOpen, message, color} = props
    const vertical = "top"
    const horizontal = "center"

    function handleClose() {
      setOpen(false)
    }

    return (
      <Snackbar
          anchorOrigin={{ vertical, horizontal }}
          open={open}
          onClose={handleClose}
          key={vertical + horizontal}
      >
        <SnackbarContent 
          message={message}
          sx={{backgroundColor: color}}
        />
      </Snackbar>
    )
}
